# Vue.js Workshop

> The entire code from the Vue.js workshop held on 02.07.2017

## Build Setup

``` bash
# install npm
npm install vue-cli

#initialize the project
vue init template_name('webpack' in this case) name

? Project name (garb) - name of the project
? Project description (A Vue.js project) - description of the project
? Author (Ravliox <IstrateE@gmail.com>) - usually your github or gitlab username
❯ Runtime + Compiler: recommended for most users 
  Runtime-only: about 6KB lighter min+gzip, but templates (or any Vue-specific H
TML) are ONLY allowed in .vue files - render functions are required elsewhere 

# We'll leave this as default for now

? Install vue-router? (Y/n) - Yes
# And no's from now on

# enter your project directory
cd name

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
